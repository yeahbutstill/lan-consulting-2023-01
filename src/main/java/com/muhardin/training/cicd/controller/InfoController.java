package com.muhardin.training.cicd.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.GitProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {

    @Autowired private GitProperties gitProperties;

    @GetMapping("/api/info")
    public Map<String, String> info(){
        Map<String, String> info = new HashMap<>();
        info.put("git-commit-id", gitProperties.getShortCommitId());
        info.put("git-tag", gitProperties.get("git.tags"));
        return info;
    }
}
