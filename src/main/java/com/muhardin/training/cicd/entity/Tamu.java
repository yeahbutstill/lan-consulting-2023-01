package com.muhardin.training.cicd.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity @Data
public class Tamu {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull @NotEmpty
    private String email;

    @NotNull @NotEmpty
    private String nama;
}
