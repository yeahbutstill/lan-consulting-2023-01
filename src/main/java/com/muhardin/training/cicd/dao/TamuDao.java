package com.muhardin.training.cicd.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.muhardin.training.cicd.entity.Tamu;

public interface TamuDao extends PagingAndSortingRepository<Tamu, Integer> {
    List<Tamu> findByNamaContainingIgnoreCase(String nama);
    List<Tamu> findByNamaContaining(String nama);
}
