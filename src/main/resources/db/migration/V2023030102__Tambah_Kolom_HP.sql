alter table tamu 
add column no_hp varchar(50);

update tamu 
set no_hp = 'belum-diisi';

alter table tamu
modify column no_hp varchar(50) not null;