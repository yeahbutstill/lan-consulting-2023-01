package com.muhardin.training.cicd.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class CalenderHelperTests {

    @Test
    public void testHitungHari(){
        Integer hari = CalenderHelper.hitungHari(4, 2021);
        System.out.println("Jumlah hari April 2021 : "+hari);
        assertEquals(30, hari);
        //assertEquals(31, CalenderHelper.hitungHari(1, 2023));
    }
}
