package com.muhardin.training.cicd.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(
    scripts = {
        "classpath:/sql/clear-data-tamu.sql",
        "classpath:/sql/sample-data-tamu.sql"
    }
)
public class TamuDaoTests {

    @Autowired private TamuDao tamuDao;

    @Test
    public void testCariNamaIgnoreCase(){
        assertEquals(5, tamuDao.findByNamaContainingIgnoreCase("user").size());
        assertEquals(5, tamuDao.findByNamaContainingIgnoreCase("USER").size());
        assertEquals(0, tamuDao.findByNamaContainingIgnoreCase("user 001").size());
        assertEquals(1, tamuDao.findByNamaContainingIgnoreCase("USER 901").size());
        assertEquals(1, tamuDao.findByNamaContainingIgnoreCase("User 902").size());
        assertEquals(5, tamuDao.findByNamaContainingIgnoreCase("ser").size());
    }
}
