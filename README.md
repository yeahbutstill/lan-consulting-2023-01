# Aplikasi Bukutamu #

1. Menjalankan database development mysql di local dengan docker

    ```
    docker run --rm --name db-bukutamu -e MYSQL_RANDOM_ROOT_PASSWORD=yes -e MYSQL_DATABASE=db-bukutamu -e MYSQL_USER=bukutamu -e MYSQL_PASSWORD=bukutamu123 -p 3306:3306 -v db-bukutamu:/var/lib/mysql mysql:8
    ```

2. Menggunakan docker compose

    ```
    docker-compose up
    ```

3. Menjalankan aplikasi

    ```
    mvn clean spring-boot:run
    ```

## Menyiapkan database MySQL di server ##

1. Create user

    ```
    create user `bukutamu-production`@localhost identified by 'HH6td60NnqZzYEb2gttv';
    ```

2. Berikan permission untuk database bukutamu

    ```
    grant all on `db-bukutamu`.* to `bukutamu-production`@localhost;
    ```

3. Buat database

    ```
    create database `db-bukutamu`;
    ```

## Setup SSH passwordless-login untuk Gitlab-CI deployment ##

1. Generate SSH Keypair

    ```
    ssh-keygen -f gitlab 
    ```

    Nanti akan diminta passphrase, kosongkan saja.

    ```
    Generating public/private rsa key pair.
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in gitlab
    Your public key has been saved in gitlab.pub
    The key fingerprint is:
    SHA256:/+4q3njUItOz+dhfzf7/R68JrDx7FYX2W0PAYEAAA6s endymuhardin@Endys-MacBook-Pro.local
    The key's randomart image is:
    +---[RSA 3072]----+
    |   ..o..oo.oo... |
    |    . .   .  .o..|
    |   .         ..o |
    |  .           ..o|
    | E      S. .   .+|
    |        o.=.. .oo|
    |         +.=o. .=|
    |        .+==.. +o|
    |       .ooBO*.+.X|
    +----[SHA256]-----+
    ```

    Ini akan menghasilkan 2 file :

    * `gitlab` : private key
    * `gitlab.pub` : public key

2. Tambahkan isi file `gitlab.pub` ke file `/root/.ssh/authorized_keys` di server

3. Tambahkan isi file `gitlab` di repository gitlab, bagian Settings > CI/CD > Variables

    [![Gitlab Private Key](docs/img/gitlab-private-key.png)](docs/img/gitlab-private-key.png)